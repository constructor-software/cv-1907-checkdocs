﻿namespace Constructor.WinUI
{
    partial class SelectServerDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition3 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition4 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition5 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectServerDialog));
            this.layoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.serverNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.progressContainerPanel = new DevExpress.XtraEditors.PanelControl();
            this.progressPanel = new DevExpress.XtraWaitForm.ProgressPanel();
            this.serverImageListBox = new DevExpress.XtraEditors.ImageListBoxControl();
            this.instanceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.promptLabel = new DevExpress.XtraEditors.LabelControl();
            this.cancelButton = new DevExpress.XtraEditors.SimpleButton();
            this.okButton = new DevExpress.XtraEditors.SimpleButton();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.promptLayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.serverLayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.actionLayoutGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.okLayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.cancelLayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.progressLayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.serverNameLayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).BeginInit();
            this.layoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.serverNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressContainerPanel)).BeginInit();
            this.progressContainerPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.serverImageListBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.instanceBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.promptLayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.serverLayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.actionLayoutGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.okLayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelLayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressLayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.serverNameLayoutItem)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl
            // 
            this.layoutControl.AllowCustomization = false;
            this.layoutControl.Controls.Add(this.serverNameTextEdit);
            this.layoutControl.Controls.Add(this.progressContainerPanel);
            this.layoutControl.Controls.Add(this.serverImageListBox);
            this.layoutControl.Controls.Add(this.promptLabel);
            this.layoutControl.Controls.Add(this.cancelButton);
            this.layoutControl.Controls.Add(this.okButton);
            this.layoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl.Location = new System.Drawing.Point(0, 0);
            this.layoutControl.Name = "layoutControl";
            this.layoutControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(807, 234, 650, 400);
            this.layoutControl.Root = this.Root;
            this.layoutControl.Size = new System.Drawing.Size(320, 475);
            this.layoutControl.TabIndex = 9;
            // 
            // serverNameTextEdit
            // 
            this.serverNameTextEdit.Location = new System.Drawing.Point(12, 409);
            this.serverNameTextEdit.Name = "serverNameTextEdit";
            this.serverNameTextEdit.Properties.AdvancedModeOptions.Label = "Server";
            this.serverNameTextEdit.Size = new System.Drawing.Size(296, 20);
            this.serverNameTextEdit.StyleController = this.layoutControl;
            this.serverNameTextEdit.TabIndex = 12;
            // 
            // progressContainerPanel
            // 
            this.progressContainerPanel.Controls.Add(this.progressPanel);
            this.progressContainerPanel.Location = new System.Drawing.Point(12, 349);
            this.progressContainerPanel.MinimumSize = new System.Drawing.Size(0, 40);
            this.progressContainerPanel.Name = "progressContainerPanel";
            this.progressContainerPanel.Size = new System.Drawing.Size(296, 40);
            this.progressContainerPanel.TabIndex = 11;
            // 
            // progressPanel
            // 
            this.progressPanel.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.progressPanel.Appearance.Options.UseBackColor = true;
            this.progressPanel.ContentAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.progressPanel.Description = "Searching for SQL Server instances...";
            this.progressPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progressPanel.Location = new System.Drawing.Point(2, 2);
            this.progressPanel.Name = "progressPanel";
            this.progressPanel.Size = new System.Drawing.Size(292, 36);
            this.progressPanel.TabIndex = 0;
            // 
            // serverImageListBox
            // 
            this.serverImageListBox.DataSource = this.instanceBindingSource;
            this.serverImageListBox.DisplayMember = "DisplayName";
            this.serverImageListBox.ImageIndexMember = "DisplayImageIndex";
            this.serverImageListBox.Location = new System.Drawing.Point(12, 39);
            this.serverImageListBox.Name = "serverImageListBox";
            this.serverImageListBox.Size = new System.Drawing.Size(296, 306);
            this.serverImageListBox.StyleController = this.layoutControl;
            this.serverImageListBox.TabIndex = 9;
            this.serverImageListBox.SelectedIndexChanged += new System.EventHandler(this.serverImageListBox_SelectedIndexChanged);
            this.serverImageListBox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.serverImageListBox_MouseDoubleClick);
            // 
            // instanceBindingSource
            // 
            this.instanceBindingSource.DataSource = typeof(Constructor.WinUI.SelectServerDialog.SqlServerInstance);
            // 
            // promptLabel
            // 
            this.promptLabel.Appearance.ForeColor = DevExpress.LookAndFeel.DXSkinColors.ForeColors.Question;
            this.promptLabel.Appearance.Options.UseForeColor = true;
            this.promptLabel.Appearance.Options.UseTextOptions = true;
            this.promptLabel.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.promptLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.promptLabel.Location = new System.Drawing.Point(12, 12);
            this.promptLabel.Name = "promptLabel";
            this.promptLabel.Size = new System.Drawing.Size(296, 13);
            this.promptLabel.StyleController = this.layoutControl;
            this.promptLabel.TabIndex = 6;
            this.promptLabel.Text = "Please select from the following list of SQL Server instances:";
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(166, 441);
            this.cancelButton.MaximumSize = new System.Drawing.Size(80, 0);
            this.cancelButton.MinimumSize = new System.Drawing.Size(80, 0);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(80, 22);
            this.cancelButton.StyleController = this.layoutControl;
            this.cancelButton.TabIndex = 7;
            this.cancelButton.Text = "&Cancel";
            // 
            // okButton
            // 
            this.okButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.okButton.Location = new System.Drawing.Point(74, 441);
            this.okButton.MaximumSize = new System.Drawing.Size(80, 0);
            this.okButton.MinimumSize = new System.Drawing.Size(80, 0);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(80, 22);
            this.okButton.StyleController = this.layoutControl;
            this.okButton.TabIndex = 8;
            this.okButton.Text = "&Ok";
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.promptLayoutItem,
            this.serverLayoutItem,
            this.actionLayoutGroup,
            this.progressLayoutItem,
            this.emptySpaceItem1,
            this.serverNameLayoutItem});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(320, 475);
            this.Root.TextVisible = false;
            // 
            // promptLayoutItem
            // 
            this.promptLayoutItem.ContentHorzAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.promptLayoutItem.ContentVertAlignment = DevExpress.Utils.VertAlignment.Center;
            this.promptLayoutItem.Control = this.promptLabel;
            this.promptLayoutItem.Location = new System.Drawing.Point(0, 0);
            this.promptLayoutItem.Name = "promptLayoutItem";
            this.promptLayoutItem.Size = new System.Drawing.Size(300, 17);
            this.promptLayoutItem.Text = "Prompt";
            this.promptLayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.promptLayoutItem.TextVisible = false;
            // 
            // serverLayoutItem
            // 
            this.serverLayoutItem.Control = this.serverImageListBox;
            this.serverLayoutItem.Location = new System.Drawing.Point(0, 27);
            this.serverLayoutItem.MinSize = new System.Drawing.Size(54, 4);
            this.serverLayoutItem.Name = "serverLayoutItem";
            this.serverLayoutItem.Size = new System.Drawing.Size(300, 310);
            this.serverLayoutItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.serverLayoutItem.Text = "Servers";
            this.serverLayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.serverLayoutItem.TextVisible = false;
            this.serverLayoutItem.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // actionLayoutGroup
            // 
            this.actionLayoutGroup.GroupBordersVisible = false;
            this.actionLayoutGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.okLayoutItem,
            this.cancelLayoutItem});
            this.actionLayoutGroup.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.actionLayoutGroup.Location = new System.Drawing.Point(0, 421);
            this.actionLayoutGroup.Name = "actionLayoutGroup";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 50D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.AutoSize;
            columnDefinition2.Width = 84D;
            columnDefinition3.SizeType = System.Windows.Forms.SizeType.Absolute;
            columnDefinition3.Width = 8D;
            columnDefinition4.SizeType = System.Windows.Forms.SizeType.AutoSize;
            columnDefinition4.Width = 84D;
            columnDefinition5.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition5.Width = 50D;
            this.actionLayoutGroup.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2,
            columnDefinition3,
            columnDefinition4,
            columnDefinition5});
            rowDefinition1.Height = 8D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.Absolute;
            rowDefinition2.Height = 26D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.AutoSize;
            this.actionLayoutGroup.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2});
            this.actionLayoutGroup.Size = new System.Drawing.Size(300, 34);
            this.actionLayoutGroup.Text = "Action";
            this.actionLayoutGroup.TextVisible = false;
            // 
            // okLayoutItem
            // 
            this.okLayoutItem.Control = this.okButton;
            this.okLayoutItem.Location = new System.Drawing.Point(62, 8);
            this.okLayoutItem.Name = "okLayoutItem";
            this.okLayoutItem.OptionsTableLayoutItem.ColumnIndex = 1;
            this.okLayoutItem.OptionsTableLayoutItem.RowIndex = 1;
            this.okLayoutItem.Size = new System.Drawing.Size(84, 26);
            this.okLayoutItem.Text = "OK";
            this.okLayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.okLayoutItem.TextVisible = false;
            // 
            // cancelLayoutItem
            // 
            this.cancelLayoutItem.Control = this.cancelButton;
            this.cancelLayoutItem.Location = new System.Drawing.Point(154, 8);
            this.cancelLayoutItem.Name = "cancelLayoutItem";
            this.cancelLayoutItem.OptionsTableLayoutItem.ColumnIndex = 3;
            this.cancelLayoutItem.OptionsTableLayoutItem.RowIndex = 1;
            this.cancelLayoutItem.Size = new System.Drawing.Size(84, 26);
            this.cancelLayoutItem.Text = "Cancel";
            this.cancelLayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.cancelLayoutItem.TextVisible = false;
            // 
            // progressLayoutItem
            // 
            this.progressLayoutItem.Control = this.progressContainerPanel;
            this.progressLayoutItem.Location = new System.Drawing.Point(0, 337);
            this.progressLayoutItem.MinSize = new System.Drawing.Size(5, 44);
            this.progressLayoutItem.Name = "progressLayoutItem";
            this.progressLayoutItem.Size = new System.Drawing.Size(300, 44);
            this.progressLayoutItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.progressLayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.progressLayoutItem.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 17);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(300, 10);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // serverNameLayoutItem
            // 
            this.serverNameLayoutItem.Control = this.serverNameTextEdit;
            this.serverNameLayoutItem.Location = new System.Drawing.Point(0, 381);
            this.serverNameLayoutItem.Name = "serverNameLayoutItem";
            this.serverNameLayoutItem.Size = new System.Drawing.Size(300, 40);
            this.serverNameLayoutItem.Text = "Server Name:";
            this.serverNameLayoutItem.TextLocation = DevExpress.Utils.Locations.Top;
            this.serverNameLayoutItem.TextSize = new System.Drawing.Size(66, 13);
            // 
            // SelectServerDialog
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(320, 475);
            this.Controls.Add(this.layoutControl);
            this.IconOptions.Icon = ((System.Drawing.Icon)(resources.GetObject("SelectServerDialog.IconOptions.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SelectServerDialog";
            this.Text = "Find Servers";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).EndInit();
            this.layoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.serverNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressContainerPanel)).EndInit();
            this.progressContainerPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.serverImageListBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.instanceBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.promptLayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.serverLayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.actionLayoutGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.okLayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cancelLayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressLayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.serverNameLayoutItem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraLayout.LayoutControl layoutControl;
        private DevExpress.XtraEditors.SimpleButton cancelButton;
        private DevExpress.XtraEditors.SimpleButton okButton;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlItem okLayoutItem;
        private DevExpress.XtraLayout.LayoutControlItem cancelLayoutItem;
        private DevExpress.XtraEditors.ImageListBoxControl serverImageListBox;
        private DevExpress.XtraLayout.LayoutControlItem serverLayoutItem;
        private DevExpress.XtraLayout.LayoutControlGroup actionLayoutGroup;
        private System.Windows.Forms.BindingSource instanceBindingSource;
        private DevExpress.XtraEditors.PanelControl progressContainerPanel;
        private DevExpress.XtraWaitForm.ProgressPanel progressPanel;
        private DevExpress.XtraLayout.LayoutControlItem progressLayoutItem;
        private DevExpress.XtraEditors.LabelControl promptLabel;
        private DevExpress.XtraLayout.LayoutControlItem promptLayoutItem;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.TextEdit serverNameTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem serverNameLayoutItem;
    }
}