﻿
namespace CheckDocs
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Data.UnboundSourceProperty unboundSourceProperty1 = new DevExpress.Data.UnboundSourceProperty();
            DevExpress.Data.UnboundSourceProperty unboundSourceProperty2 = new DevExpress.Data.UnboundSourceProperty();
            DevExpress.Data.UnboundSourceProperty unboundSourceProperty3 = new DevExpress.Data.UnboundSourceProperty();
            DevExpress.Data.UnboundSourceProperty unboundSourceProperty4 = new DevExpress.Data.UnboundSourceProperty();
            DevExpress.Data.UnboundSourceProperty unboundSourceProperty5 = new DevExpress.Data.UnboundSourceProperty();
            DevExpress.Data.UnboundSourceProperty unboundSourceProperty6 = new DevExpress.Data.UnboundSourceProperty();
            DevExpress.Data.UnboundSourceProperty unboundSourceProperty7 = new DevExpress.Data.UnboundSourceProperty();
            DevExpress.Data.UnboundSourceProperty unboundSourceProperty8 = new DevExpress.Data.UnboundSourceProperty();
            DevExpress.Data.UnboundSourceProperty unboundSourceProperty9 = new DevExpress.Data.UnboundSourceProperty();
            DevExpress.Data.UnboundSourceProperty unboundSourceProperty10 = new DevExpress.Data.UnboundSourceProperty();
            DevExpress.Data.UnboundSourceProperty unboundSourceProperty11 = new DevExpress.Data.UnboundSourceProperty();
            DevExpress.Data.UnboundSourceProperty unboundSourceProperty12 = new DevExpress.Data.UnboundSourceProperty();
            DevExpress.Data.UnboundSourceProperty unboundSourceProperty13 = new DevExpress.Data.UnboundSourceProperty();
            DevExpress.Data.UnboundSourceProperty unboundSourceProperty14 = new DevExpress.Data.UnboundSourceProperty();
            DevExpress.Data.UnboundSourceProperty unboundSourceProperty15 = new DevExpress.Data.UnboundSourceProperty();
            DevExpress.Data.UnboundSourceProperty unboundSourceProperty16 = new DevExpress.Data.UnboundSourceProperty();
            DevExpress.Data.UnboundSourceProperty unboundSourceProperty17 = new DevExpress.Data.UnboundSourceProperty();
            DevExpress.Data.UnboundSourceProperty unboundSourceProperty18 = new DevExpress.Data.UnboundSourceProperty();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition1 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition2 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.ColumnDefinition columnDefinition3 = new DevExpress.XtraLayout.ColumnDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition1 = new DevExpress.XtraLayout.RowDefinition();
            DevExpress.XtraLayout.RowDefinition rowDefinition2 = new DevExpress.XtraLayout.RowDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.layoutControl = new DevExpress.XtraLayout.LayoutControl();
            this.fieldGrid = new DevExpress.XtraGrid.GridControl();
            this.fieldGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.fieldIDColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.fieldNameColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.fieldTextColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.fieldStartColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.fieldLengthColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.toolbarFormManager = new DevExpress.XtraBars.ToolbarForm.ToolbarFormManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.saveOrigBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.saveDocxBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.statusBarStaticItem = new DevExpress.XtraBars.BarStaticItem();
            this.statusBarEditItem = new DevExpress.XtraBars.BarEditItem();
            this.statusMarqueeProgressBar = new DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar();
            this.serverBarButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.vertBarCheckItem = new DevExpress.XtraBars.BarCheckItem();
            this.horzBarCheckItem = new DevExpress.XtraBars.BarCheckItem();
            this.hexGrid = new DevExpress.XtraGrid.GridControl();
            this.hexSource = new DevExpress.Data.UnboundSource(this.components);
            this.hexGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPosition = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colByte0 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colByte1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colByte2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colByte3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colByte4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colByte5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colByte6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colByte7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colByte8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colByte9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colByteA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colByteB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colByteC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colByteD = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colByteE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colByteF = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colText = new DevExpress.XtraGrid.Columns.GridColumn();
            this.grid = new DevExpress.XtraGrid.GridControl();
            this.docsSource = new DevExpress.Data.Linq.EntityInstantFeedbackSource();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colFilename = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCreateDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContextMode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsTemplate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSize = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompressed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colHeader = new DevExpress.XtraGrid.Columns.GridColumn();
            this.headerRepositoryItemTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.docxTextControl = new TXTextControl.TextControl();
            this.txTextControl = new TXTextControl.TextControl();
            this.richEdit = new DevExpress.XtraRichEdit.RichEditControl();
            this.layoutGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.gridLayoutGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.gridLayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem = new DevExpress.XtraLayout.SplitterItem();
            this.docLayoutGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.docContentLayoutGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.txLayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.docxLayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.richEditLayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.txLabelItem = new DevExpress.XtraLayout.SimpleLabelItem();
            this.docxLabelItem = new DevExpress.XtraLayout.SimpleLabelItem();
            this.richEditLabelItem = new DevExpress.XtraLayout.SimpleLabelItem();
            this.docSplitterItem = new DevExpress.XtraLayout.SplitterItem();
            this.infoLayoutGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.hexLayoutGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.hexLayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.fieldLayoutGroup = new DevExpress.XtraLayout.LayoutControlGroup();
            this.fieldLayoutItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.infoSplitterItem = new DevExpress.XtraLayout.SplitterItem();
            this.toolbarFormControl1 = new DevExpress.XtraBars.ToolbarForm.ToolbarFormControl();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).BeginInit();
            this.layoutControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fieldGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fieldGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toolbarFormManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusMarqueeProgressBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hexGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hexSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hexGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.headerRepositoryItemTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLayoutGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.docLayoutGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.docContentLayoutGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txLayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.docxLayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.richEditLayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txLabelItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.docxLabelItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.richEditLabelItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.docSplitterItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.infoLayoutGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hexLayoutGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hexLayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fieldLayoutGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fieldLayoutItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.infoSplitterItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.toolbarFormControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl
            // 
            this.layoutControl.Controls.Add(this.fieldGrid);
            this.layoutControl.Controls.Add(this.hexGrid);
            this.layoutControl.Controls.Add(this.grid);
            this.layoutControl.Controls.Add(this.docxTextControl);
            this.layoutControl.Controls.Add(this.txTextControl);
            this.layoutControl.Controls.Add(this.richEdit);
            this.layoutControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl.Location = new System.Drawing.Point(0, 30);
            this.layoutControl.Name = "layoutControl";
            this.layoutControl.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(903, 403, 650, 400);
            this.layoutControl.Root = this.layoutGroup;
            this.layoutControl.Size = new System.Drawing.Size(1200, 540);
            this.layoutControl.TabIndex = 0;
            this.layoutControl.Text = "layoutControl1";
            // 
            // fieldGrid
            // 
            this.fieldGrid.DataSource = typeof(TXTextControl.TextField);
            this.fieldGrid.Location = new System.Drawing.Point(40, 231);
            this.fieldGrid.MainView = this.fieldGridView;
            this.fieldGrid.MenuManager = this.toolbarFormManager;
            this.fieldGrid.Name = "fieldGrid";
            this.fieldGrid.Size = new System.Drawing.Size(1146, 90);
            this.fieldGrid.TabIndex = 9;
            this.fieldGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.fieldGridView});
            // 
            // fieldGridView
            // 
            this.fieldGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.fieldIDColumn,
            this.fieldNameColumn,
            this.fieldTextColumn,
            this.fieldStartColumn,
            this.fieldLengthColumn});
            this.fieldGridView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.fieldGridView.GridControl = this.fieldGrid;
            this.fieldGridView.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.fieldGridView.Name = "fieldGridView";
            this.fieldGridView.OptionsBehavior.Editable = false;
            this.fieldGridView.OptionsBehavior.ReadOnly = true;
            this.fieldGridView.OptionsFilter.AllowFilterEditor = false;
            this.fieldGridView.OptionsFind.AllowFindPanel = false;
            this.fieldGridView.OptionsMenu.EnableColumnMenu = false;
            this.fieldGridView.OptionsMenu.EnableFooterMenu = false;
            this.fieldGridView.OptionsMenu.EnableGroupPanelMenu = false;
            this.fieldGridView.OptionsMenu.EnableGroupRowMenu = true;
            this.fieldGridView.OptionsNavigation.UseTabKey = false;
            this.fieldGridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.fieldGridView.OptionsSelection.MultiSelect = true;
            this.fieldGridView.OptionsView.ColumnAutoWidth = false;
            this.fieldGridView.OptionsView.ShowGroupPanel = false;
            this.fieldGridView.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.fieldIDColumn, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.fieldGridView.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            // 
            // fieldIDColumn
            // 
            this.fieldIDColumn.AppearanceHeader.Options.UseTextOptions = true;
            this.fieldIDColumn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.fieldIDColumn.FieldName = "ID";
            this.fieldIDColumn.Name = "fieldIDColumn";
            this.fieldIDColumn.Visible = true;
            this.fieldIDColumn.VisibleIndex = 0;
            this.fieldIDColumn.Width = 60;
            // 
            // fieldNameColumn
            // 
            this.fieldNameColumn.FieldName = "Name";
            this.fieldNameColumn.Name = "fieldNameColumn";
            this.fieldNameColumn.Visible = true;
            this.fieldNameColumn.VisibleIndex = 1;
            this.fieldNameColumn.Width = 360;
            // 
            // fieldTextColumn
            // 
            this.fieldTextColumn.FieldName = "Text";
            this.fieldTextColumn.Name = "fieldTextColumn";
            this.fieldTextColumn.Visible = true;
            this.fieldTextColumn.VisibleIndex = 2;
            this.fieldTextColumn.Width = 360;
            // 
            // fieldStartColumn
            // 
            this.fieldStartColumn.AppearanceHeader.Options.UseTextOptions = true;
            this.fieldStartColumn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.fieldStartColumn.FieldName = "Start";
            this.fieldStartColumn.Name = "fieldStartColumn";
            this.fieldStartColumn.Visible = true;
            this.fieldStartColumn.VisibleIndex = 3;
            this.fieldStartColumn.Width = 60;
            // 
            // fieldLengthColumn
            // 
            this.fieldLengthColumn.AppearanceHeader.Options.UseTextOptions = true;
            this.fieldLengthColumn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.fieldLengthColumn.FieldName = "Length";
            this.fieldLengthColumn.Name = "fieldLengthColumn";
            this.fieldLengthColumn.Visible = true;
            this.fieldLengthColumn.VisibleIndex = 4;
            this.fieldLengthColumn.Width = 60;
            // 
            // toolbarFormManager
            // 
            this.toolbarFormManager.DockControls.Add(this.barDockControlTop);
            this.toolbarFormManager.DockControls.Add(this.barDockControlBottom);
            this.toolbarFormManager.DockControls.Add(this.barDockControlLeft);
            this.toolbarFormManager.DockControls.Add(this.barDockControlRight);
            this.toolbarFormManager.Form = this;
            this.toolbarFormManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.saveOrigBarButtonItem,
            this.saveDocxBarButtonItem,
            this.statusBarStaticItem,
            this.statusBarEditItem,
            this.serverBarButtonItem,
            this.vertBarCheckItem,
            this.horzBarCheckItem});
            this.toolbarFormManager.MaxItemId = 7;
            this.toolbarFormManager.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.statusMarqueeProgressBar});
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 30);
            this.barDockControlTop.Manager = this.toolbarFormManager;
            this.barDockControlTop.Size = new System.Drawing.Size(1200, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 570);
            this.barDockControlBottom.Manager = this.toolbarFormManager;
            this.barDockControlBottom.Size = new System.Drawing.Size(1200, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 30);
            this.barDockControlLeft.Manager = this.toolbarFormManager;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 540);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1200, 30);
            this.barDockControlRight.Manager = this.toolbarFormManager;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 540);
            // 
            // saveOrigBarButtonItem
            // 
            this.saveOrigBarButtonItem.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.saveOrigBarButtonItem.Caption = "Save Original";
            this.saveOrigBarButtonItem.Enabled = false;
            this.saveOrigBarButtonItem.Id = 0;
            this.saveOrigBarButtonItem.ImageOptions.ImageUri.Uri = "New;Size16x16";
            this.saveOrigBarButtonItem.Name = "saveOrigBarButtonItem";
            this.saveOrigBarButtonItem.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.saveOrigBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.saveOrigBarButtonItem_ItemClick);
            // 
            // saveDocxBarButtonItem
            // 
            this.saveDocxBarButtonItem.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.saveDocxBarButtonItem.Caption = "Save DOCX";
            this.saveDocxBarButtonItem.Enabled = false;
            this.saveDocxBarButtonItem.Id = 1;
            this.saveDocxBarButtonItem.ImageOptions.ImageUri.Uri = "ExportToDOCX;Size16x16";
            this.saveDocxBarButtonItem.Name = "saveDocxBarButtonItem";
            this.saveDocxBarButtonItem.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.saveDocxBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.saveDocxBarButtonItem_ItemClick);
            // 
            // statusBarStaticItem
            // 
            this.statusBarStaticItem.AllowHtmlText = DevExpress.Utils.DefaultBoolean.True;
            this.statusBarStaticItem.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring;
            this.statusBarStaticItem.Caption = "Status";
            this.statusBarStaticItem.Id = 2;
            this.statusBarStaticItem.Name = "statusBarStaticItem";
            this.statusBarStaticItem.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // statusBarEditItem
            // 
            this.statusBarEditItem.Caption = "Status";
            this.statusBarEditItem.Edit = this.statusMarqueeProgressBar;
            this.statusBarEditItem.Id = 3;
            this.statusBarEditItem.Name = "statusBarEditItem";
            this.statusBarEditItem.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // statusMarqueeProgressBar
            // 
            this.statusMarqueeProgressBar.Name = "statusMarqueeProgressBar";
            this.statusMarqueeProgressBar.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
            // 
            // serverBarButtonItem
            // 
            this.serverBarButtonItem.Caption = "SERVER\\INSTANCE";
            this.serverBarButtonItem.Id = 4;
            this.serverBarButtonItem.ImageOptions.ImageUri.Uri = "EditDataSource;Size16x16";
            this.serverBarButtonItem.Name = "serverBarButtonItem";
            this.serverBarButtonItem.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.serverBarButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.serverBarButtonItem_ItemClick);
            // 
            // vertBarCheckItem
            // 
            this.vertBarCheckItem.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.vertBarCheckItem.Caption = "Vertical View";
            this.vertBarCheckItem.CheckStyle = DevExpress.XtraBars.BarCheckStyles.Radio;
            this.vertBarCheckItem.GroupIndex = 1;
            this.vertBarCheckItem.Id = 5;
            this.vertBarCheckItem.ImageOptions.ImageUri.Uri = "dashboards/arrangeinrows;Size16x16";
            this.vertBarCheckItem.Name = "vertBarCheckItem";
            this.vertBarCheckItem.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.vertBarCheckItem_CheckedChanged);
            // 
            // horzBarCheckItem
            // 
            this.horzBarCheckItem.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.horzBarCheckItem.BindableChecked = true;
            this.horzBarCheckItem.Caption = "Horizontal View";
            this.horzBarCheckItem.Checked = true;
            this.horzBarCheckItem.CheckStyle = DevExpress.XtraBars.BarCheckStyles.Radio;
            this.horzBarCheckItem.GroupIndex = 1;
            this.horzBarCheckItem.Id = 6;
            this.horzBarCheckItem.ImageOptions.ImageUri.Uri = "dashboards/arrangeincolumns;Size16x16";
            this.horzBarCheckItem.Name = "horzBarCheckItem";
            this.horzBarCheckItem.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.horzBarCheckItem_CheckedChanged);
            // 
            // hexGrid
            // 
            this.hexGrid.DataSource = this.hexSource;
            this.hexGrid.Location = new System.Drawing.Point(40, 169);
            this.hexGrid.MainView = this.hexGridView;
            this.hexGrid.MenuManager = this.toolbarFormManager;
            this.hexGrid.Name = "hexGrid";
            this.hexGrid.Size = new System.Drawing.Size(1146, 44);
            this.hexGrid.TabIndex = 8;
            this.hexGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.hexGridView});
            // 
            // hexSource
            // 
            unboundSourceProperty1.DisplayName = null;
            unboundSourceProperty1.Name = "Position";
            unboundSourceProperty1.PropertyType = typeof(int);
            unboundSourceProperty2.DisplayName = null;
            unboundSourceProperty2.Name = "Byte0";
            unboundSourceProperty2.PropertyType = typeof(byte);
            unboundSourceProperty3.DisplayName = null;
            unboundSourceProperty3.Name = "Byte1";
            unboundSourceProperty3.PropertyType = typeof(byte);
            unboundSourceProperty4.DisplayName = null;
            unboundSourceProperty4.Name = "Byte2";
            unboundSourceProperty4.PropertyType = typeof(byte);
            unboundSourceProperty5.DisplayName = null;
            unboundSourceProperty5.Name = "Byte3";
            unboundSourceProperty5.PropertyType = typeof(byte);
            unboundSourceProperty6.DisplayName = null;
            unboundSourceProperty6.Name = "Byte4";
            unboundSourceProperty6.PropertyType = typeof(byte);
            unboundSourceProperty7.DisplayName = null;
            unboundSourceProperty7.Name = "Byte5";
            unboundSourceProperty7.PropertyType = typeof(byte);
            unboundSourceProperty8.DisplayName = null;
            unboundSourceProperty8.Name = "Byte6";
            unboundSourceProperty8.PropertyType = typeof(byte);
            unboundSourceProperty9.DisplayName = null;
            unboundSourceProperty9.Name = "Byte7";
            unboundSourceProperty9.PropertyType = typeof(byte);
            unboundSourceProperty10.DisplayName = null;
            unboundSourceProperty10.Name = "Byte8";
            unboundSourceProperty10.PropertyType = typeof(byte);
            unboundSourceProperty11.DisplayName = null;
            unboundSourceProperty11.Name = "Byte9";
            unboundSourceProperty11.PropertyType = typeof(byte);
            unboundSourceProperty12.DisplayName = null;
            unboundSourceProperty12.Name = "ByteA";
            unboundSourceProperty12.PropertyType = typeof(byte);
            unboundSourceProperty13.DisplayName = null;
            unboundSourceProperty13.Name = "ByteB";
            unboundSourceProperty13.PropertyType = typeof(byte);
            unboundSourceProperty14.DisplayName = null;
            unboundSourceProperty14.Name = "ByteC";
            unboundSourceProperty14.PropertyType = typeof(byte);
            unboundSourceProperty15.DisplayName = null;
            unboundSourceProperty15.Name = "ByteD";
            unboundSourceProperty15.PropertyType = typeof(byte);
            unboundSourceProperty16.DisplayName = null;
            unboundSourceProperty16.Name = "ByteE";
            unboundSourceProperty16.PropertyType = typeof(byte);
            unboundSourceProperty17.DisplayName = null;
            unboundSourceProperty17.Name = "ByteF";
            unboundSourceProperty17.PropertyType = typeof(byte);
            unboundSourceProperty18.DisplayName = null;
            unboundSourceProperty18.Name = "Text";
            unboundSourceProperty18.PropertyType = typeof(string);
            this.hexSource.Properties.Add(unboundSourceProperty1);
            this.hexSource.Properties.Add(unboundSourceProperty2);
            this.hexSource.Properties.Add(unboundSourceProperty3);
            this.hexSource.Properties.Add(unboundSourceProperty4);
            this.hexSource.Properties.Add(unboundSourceProperty5);
            this.hexSource.Properties.Add(unboundSourceProperty6);
            this.hexSource.Properties.Add(unboundSourceProperty7);
            this.hexSource.Properties.Add(unboundSourceProperty8);
            this.hexSource.Properties.Add(unboundSourceProperty9);
            this.hexSource.Properties.Add(unboundSourceProperty10);
            this.hexSource.Properties.Add(unboundSourceProperty11);
            this.hexSource.Properties.Add(unboundSourceProperty12);
            this.hexSource.Properties.Add(unboundSourceProperty13);
            this.hexSource.Properties.Add(unboundSourceProperty14);
            this.hexSource.Properties.Add(unboundSourceProperty15);
            this.hexSource.Properties.Add(unboundSourceProperty16);
            this.hexSource.Properties.Add(unboundSourceProperty17);
            this.hexSource.Properties.Add(unboundSourceProperty18);
            this.hexSource.ValueNeeded += new System.EventHandler<DevExpress.Data.UnboundSourceValueNeededEventArgs>(this.hexSource_ValueNeeded);
            // 
            // hexGridView
            // 
            this.hexGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPosition,
            this.colByte0,
            this.colByte1,
            this.colByte2,
            this.colByte3,
            this.colByte4,
            this.colByte5,
            this.colByte6,
            this.colByte7,
            this.colByte8,
            this.colByte9,
            this.colByteA,
            this.colByteB,
            this.colByteC,
            this.colByteD,
            this.colByteE,
            this.colByteF,
            this.colText});
            this.hexGridView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.hexGridView.GridControl = this.hexGrid;
            this.hexGridView.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Never;
            this.hexGridView.Name = "hexGridView";
            this.hexGridView.OptionsBehavior.Editable = false;
            this.hexGridView.OptionsBehavior.ReadOnly = true;
            this.hexGridView.OptionsFilter.AllowFilterEditor = false;
            this.hexGridView.OptionsFind.AllowFindPanel = false;
            this.hexGridView.OptionsMenu.EnableColumnMenu = false;
            this.hexGridView.OptionsMenu.EnableFooterMenu = false;
            this.hexGridView.OptionsMenu.EnableGroupPanelMenu = false;
            this.hexGridView.OptionsMenu.EnableGroupRowMenu = true;
            this.hexGridView.OptionsNavigation.UseTabKey = false;
            this.hexGridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.hexGridView.OptionsSelection.MultiSelect = true;
            this.hexGridView.OptionsView.ColumnAutoWidth = false;
            this.hexGridView.OptionsView.ShowColumnHeaders = false;
            this.hexGridView.OptionsView.ShowGroupPanel = false;
            this.hexGridView.OptionsView.ShowIndicator = false;
            this.hexGridView.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            // 
            // colPosition
            // 
            this.colPosition.AppearanceCell.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colPosition.AppearanceCell.Options.UseFont = true;
            this.colPosition.DisplayFormat.FormatString = "X8";
            this.colPosition.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPosition.FieldName = "Position";
            this.colPosition.Name = "colPosition";
            this.colPosition.Visible = true;
            this.colPosition.VisibleIndex = 0;
            // 
            // colByte0
            // 
            this.colByte0.AppearanceCell.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colByte0.AppearanceCell.Options.UseFont = true;
            this.colByte0.AppearanceCell.Options.UseTextOptions = true;
            this.colByte0.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colByte0.DisplayFormat.FormatString = "X2";
            this.colByte0.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colByte0.FieldName = "Byte0";
            this.colByte0.Name = "colByte0";
            this.colByte0.Visible = true;
            this.colByte0.VisibleIndex = 1;
            this.colByte0.Width = 28;
            // 
            // colByte1
            // 
            this.colByte1.AppearanceCell.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colByte1.AppearanceCell.Options.UseFont = true;
            this.colByte1.AppearanceCell.Options.UseTextOptions = true;
            this.colByte1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colByte1.DisplayFormat.FormatString = "X2";
            this.colByte1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colByte1.FieldName = "Byte1";
            this.colByte1.Name = "colByte1";
            this.colByte1.Visible = true;
            this.colByte1.VisibleIndex = 2;
            this.colByte1.Width = 28;
            // 
            // colByte2
            // 
            this.colByte2.AppearanceCell.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colByte2.AppearanceCell.Options.UseFont = true;
            this.colByte2.AppearanceCell.Options.UseTextOptions = true;
            this.colByte2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colByte2.DisplayFormat.FormatString = "X2";
            this.colByte2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colByte2.FieldName = "Byte2";
            this.colByte2.Name = "colByte2";
            this.colByte2.Visible = true;
            this.colByte2.VisibleIndex = 3;
            this.colByte2.Width = 28;
            // 
            // colByte3
            // 
            this.colByte3.AppearanceCell.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colByte3.AppearanceCell.Options.UseFont = true;
            this.colByte3.AppearanceCell.Options.UseTextOptions = true;
            this.colByte3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colByte3.DisplayFormat.FormatString = "X2";
            this.colByte3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colByte3.FieldName = "Byte3";
            this.colByte3.Name = "colByte3";
            this.colByte3.Visible = true;
            this.colByte3.VisibleIndex = 4;
            this.colByte3.Width = 28;
            // 
            // colByte4
            // 
            this.colByte4.AppearanceCell.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colByte4.AppearanceCell.Options.UseFont = true;
            this.colByte4.AppearanceCell.Options.UseTextOptions = true;
            this.colByte4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colByte4.DisplayFormat.FormatString = "X2";
            this.colByte4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colByte4.FieldName = "Byte4";
            this.colByte4.Name = "colByte4";
            this.colByte4.Visible = true;
            this.colByte4.VisibleIndex = 5;
            this.colByte4.Width = 28;
            // 
            // colByte5
            // 
            this.colByte5.AppearanceCell.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colByte5.AppearanceCell.Options.UseFont = true;
            this.colByte5.AppearanceCell.Options.UseTextOptions = true;
            this.colByte5.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colByte5.DisplayFormat.FormatString = "X2";
            this.colByte5.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colByte5.FieldName = "Byte5";
            this.colByte5.Name = "colByte5";
            this.colByte5.Visible = true;
            this.colByte5.VisibleIndex = 6;
            this.colByte5.Width = 28;
            // 
            // colByte6
            // 
            this.colByte6.AppearanceCell.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colByte6.AppearanceCell.Options.UseFont = true;
            this.colByte6.AppearanceCell.Options.UseTextOptions = true;
            this.colByte6.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colByte6.DisplayFormat.FormatString = "X2";
            this.colByte6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colByte6.FieldName = "Byte6";
            this.colByte6.Name = "colByte6";
            this.colByte6.Visible = true;
            this.colByte6.VisibleIndex = 7;
            this.colByte6.Width = 28;
            // 
            // colByte7
            // 
            this.colByte7.AppearanceCell.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colByte7.AppearanceCell.Options.UseFont = true;
            this.colByte7.AppearanceCell.Options.UseTextOptions = true;
            this.colByte7.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colByte7.DisplayFormat.FormatString = "X2";
            this.colByte7.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colByte7.FieldName = "Byte7";
            this.colByte7.Name = "colByte7";
            this.colByte7.Visible = true;
            this.colByte7.VisibleIndex = 8;
            this.colByte7.Width = 28;
            // 
            // colByte8
            // 
            this.colByte8.AppearanceCell.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colByte8.AppearanceCell.Options.UseFont = true;
            this.colByte8.AppearanceCell.Options.UseTextOptions = true;
            this.colByte8.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colByte8.DisplayFormat.FormatString = "X2";
            this.colByte8.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colByte8.FieldName = "Byte8";
            this.colByte8.Name = "colByte8";
            this.colByte8.Visible = true;
            this.colByte8.VisibleIndex = 9;
            this.colByte8.Width = 28;
            // 
            // colByte9
            // 
            this.colByte9.AppearanceCell.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colByte9.AppearanceCell.Options.UseFont = true;
            this.colByte9.AppearanceCell.Options.UseTextOptions = true;
            this.colByte9.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colByte9.DisplayFormat.FormatString = "X2";
            this.colByte9.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colByte9.FieldName = "Byte9";
            this.colByte9.Name = "colByte9";
            this.colByte9.Visible = true;
            this.colByte9.VisibleIndex = 10;
            this.colByte9.Width = 28;
            // 
            // colByteA
            // 
            this.colByteA.AppearanceCell.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colByteA.AppearanceCell.Options.UseFont = true;
            this.colByteA.AppearanceCell.Options.UseTextOptions = true;
            this.colByteA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colByteA.DisplayFormat.FormatString = "X2";
            this.colByteA.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colByteA.FieldName = "ByteA";
            this.colByteA.Name = "colByteA";
            this.colByteA.Visible = true;
            this.colByteA.VisibleIndex = 11;
            this.colByteA.Width = 28;
            // 
            // colByteB
            // 
            this.colByteB.AppearanceCell.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colByteB.AppearanceCell.Options.UseFont = true;
            this.colByteB.AppearanceCell.Options.UseTextOptions = true;
            this.colByteB.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colByteB.DisplayFormat.FormatString = "X2";
            this.colByteB.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colByteB.FieldName = "ByteB";
            this.colByteB.Name = "colByteB";
            this.colByteB.Visible = true;
            this.colByteB.VisibleIndex = 12;
            this.colByteB.Width = 28;
            // 
            // colByteC
            // 
            this.colByteC.AppearanceCell.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colByteC.AppearanceCell.Options.UseFont = true;
            this.colByteC.AppearanceCell.Options.UseTextOptions = true;
            this.colByteC.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colByteC.DisplayFormat.FormatString = "X2";
            this.colByteC.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colByteC.FieldName = "ByteC";
            this.colByteC.Name = "colByteC";
            this.colByteC.Visible = true;
            this.colByteC.VisibleIndex = 13;
            this.colByteC.Width = 28;
            // 
            // colByteD
            // 
            this.colByteD.AppearanceCell.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colByteD.AppearanceCell.Options.UseFont = true;
            this.colByteD.AppearanceCell.Options.UseTextOptions = true;
            this.colByteD.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colByteD.DisplayFormat.FormatString = "X2";
            this.colByteD.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colByteD.FieldName = "ByteD";
            this.colByteD.Name = "colByteD";
            this.colByteD.Visible = true;
            this.colByteD.VisibleIndex = 14;
            this.colByteD.Width = 28;
            // 
            // colByteE
            // 
            this.colByteE.AppearanceCell.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colByteE.AppearanceCell.Options.UseFont = true;
            this.colByteE.AppearanceCell.Options.UseTextOptions = true;
            this.colByteE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colByteE.DisplayFormat.FormatString = "X2";
            this.colByteE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colByteE.FieldName = "ByteE";
            this.colByteE.Name = "colByteE";
            this.colByteE.Visible = true;
            this.colByteE.VisibleIndex = 15;
            this.colByteE.Width = 28;
            // 
            // colByteF
            // 
            this.colByteF.AppearanceCell.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colByteF.AppearanceCell.Options.UseFont = true;
            this.colByteF.AppearanceCell.Options.UseTextOptions = true;
            this.colByteF.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colByteF.DisplayFormat.FormatString = "X2";
            this.colByteF.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colByteF.FieldName = "ByteF";
            this.colByteF.Name = "colByteF";
            this.colByteF.Visible = true;
            this.colByteF.VisibleIndex = 16;
            this.colByteF.Width = 28;
            // 
            // colText
            // 
            this.colText.AppearanceCell.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colText.AppearanceCell.Options.UseFont = true;
            this.colText.FieldName = "Text";
            this.colText.Name = "colText";
            this.colText.Visible = true;
            this.colText.VisibleIndex = 17;
            this.colText.Width = 124;
            // 
            // grid
            // 
            this.grid.DataSource = this.docsSource;
            this.grid.Location = new System.Drawing.Point(12, 12);
            this.grid.MainView = this.gridView;
            this.grid.Name = "grid";
            this.grid.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.headerRepositoryItemTextEdit});
            this.grid.Size = new System.Drawing.Size(1176, 141);
            this.grid.TabIndex = 7;
            this.grid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView});
            // 
            // docsSource
            // 
            this.docsSource.AreSourceRowsThreadSafe = true;
            this.docsSource.DefaultSorting = null;
            this.docsSource.DesignTimeElementType = typeof(CheckDocs.MainForm.DocumentRow);
            this.docsSource.KeyExpression = "ID";
            this.docsSource.GetQueryable += new System.EventHandler<DevExpress.Data.Linq.GetQueryableEventArgs>(this.docsSource_GetQueryable);
            this.docsSource.DismissQueryable += new System.EventHandler<DevExpress.Data.Linq.GetQueryableEventArgs>(this.docsSource_DismissQueryable);
            // 
            // gridView
            // 
            this.gridView.AutoFillColumn = this.colFilename;
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID,
            this.colOwnerID,
            this.colCreateDate,
            this.colContextMode,
            this.colIsTemplate,
            this.colSize,
            this.colCompressed,
            this.colFilename,
            this.colHeader});
            this.gridView.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.gridView.GridControl = this.grid;
            this.gridView.Name = "gridView";
            this.gridView.OptionsBehavior.Editable = false;
            this.gridView.OptionsBehavior.ReadOnly = true;
            this.gridView.OptionsNavigation.UseTabKey = false;
            this.gridView.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView.OptionsView.ColumnAutoWidth = false;
            this.gridView.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridView.OptionsView.ShowGroupPanel = false;
            this.gridView.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView_FocusedRowChanged);
            this.gridView.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridView_CustomUnboundColumnData);
            this.gridView.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.gridView_CustomColumnDisplayText);
            // 
            // colFilename
            // 
            this.colFilename.FieldName = "Filename";
            this.colFilename.Name = "colFilename";
            this.colFilename.Visible = true;
            this.colFilename.VisibleIndex = 7;
            this.colFilename.Width = 160;
            // 
            // colID
            // 
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.Visible = true;
            this.colID.VisibleIndex = 0;
            this.colID.Width = 224;
            // 
            // colOwnerID
            // 
            this.colOwnerID.FieldName = "OwnerID";
            this.colOwnerID.Name = "colOwnerID";
            this.colOwnerID.Visible = true;
            this.colOwnerID.VisibleIndex = 1;
            this.colOwnerID.Width = 224;
            // 
            // colCreateDate
            // 
            this.colCreateDate.FieldName = "CreateDate";
            this.colCreateDate.Name = "colCreateDate";
            this.colCreateDate.Visible = true;
            this.colCreateDate.VisibleIndex = 2;
            this.colCreateDate.Width = 80;
            // 
            // colContextMode
            // 
            this.colContextMode.FieldName = "ContextMode";
            this.colContextMode.Name = "colContextMode";
            this.colContextMode.Visible = true;
            this.colContextMode.VisibleIndex = 3;
            this.colContextMode.Width = 88;
            // 
            // colIsTemplate
            // 
            this.colIsTemplate.AppearanceHeader.Options.UseTextOptions = true;
            this.colIsTemplate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIsTemplate.FieldName = "IsTemplate";
            this.colIsTemplate.Name = "colIsTemplate";
            this.colIsTemplate.Visible = true;
            this.colIsTemplate.VisibleIndex = 4;
            this.colIsTemplate.Width = 80;
            // 
            // colSize
            // 
            this.colSize.AppearanceHeader.Options.UseTextOptions = true;
            this.colSize.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.colSize.DisplayFormat.FormatString = "N0";
            this.colSize.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSize.FieldName = "Size";
            this.colSize.GroupFormat.FormatString = "N0";
            this.colSize.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSize.Name = "colSize";
            this.colSize.Visible = true;
            this.colSize.VisibleIndex = 5;
            this.colSize.Width = 88;
            // 
            // colCompressed
            // 
            this.colCompressed.AppearanceHeader.Options.UseTextOptions = true;
            this.colCompressed.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCompressed.Caption = "Compressed";
            this.colCompressed.FieldName = "colIsCompressed";
            this.colCompressed.Name = "colCompressed";
            this.colCompressed.OptionsFilter.AllowAutoFilter = false;
            this.colCompressed.OptionsFilter.AllowFilter = false;
            this.colCompressed.UnboundDataType = typeof(bool);
            this.colCompressed.Visible = true;
            this.colCompressed.VisibleIndex = 6;
            this.colCompressed.Width = 80;
            // 
            // colHeader
            // 
            this.colHeader.AppearanceCell.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colHeader.AppearanceCell.Options.UseFont = true;
            this.colHeader.Caption = "Raw Header (Hex)";
            this.colHeader.ColumnEdit = this.headerRepositoryItemTextEdit;
            this.colHeader.FieldName = "Header";
            this.colHeader.Name = "colHeader";
            this.colHeader.Visible = true;
            this.colHeader.VisibleIndex = 8;
            this.colHeader.Width = 172;
            // 
            // headerRepositoryItemTextEdit
            // 
            this.headerRepositoryItemTextEdit.AutoHeight = false;
            this.headerRepositoryItemTextEdit.Name = "headerRepositoryItemTextEdit";
            // 
            // docxTextControl
            // 
            this.docxTextControl.Font = new System.Drawing.Font("Arial", 10F);
            this.docxTextControl.Location = new System.Drawing.Point(405, 354);
            this.docxTextControl.Name = "docxTextControl";
            this.docxTextControl.Size = new System.Drawing.Size(389, 174);
            this.docxTextControl.TabIndex = 6;
            // 
            // txTextControl
            // 
            this.txTextControl.Font = new System.Drawing.Font("Arial", 10F);
            this.txTextControl.Location = new System.Drawing.Point(12, 354);
            this.txTextControl.Name = "txTextControl";
            this.txTextControl.Size = new System.Drawing.Size(389, 174);
            this.txTextControl.TabIndex = 5;
            // 
            // richEdit
            // 
            this.richEdit.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.richEdit.Location = new System.Drawing.Point(798, 354);
            this.richEdit.MenuManager = this.toolbarFormManager;
            this.richEdit.Name = "richEdit";
            this.richEdit.Options.HorizontalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this.richEdit.Options.VerticalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            this.richEdit.Size = new System.Drawing.Size(390, 174);
            this.richEdit.TabIndex = 4;
            this.richEdit.Views.SimpleView.Padding = new DevExpress.Portable.PortablePadding(0);
            this.richEdit.Views.SimpleView.WordWrap = false;
            // 
            // layoutGroup
            // 
            this.layoutGroup.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutGroup.GroupBordersVisible = false;
            this.layoutGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.gridLayoutGroup,
            this.splitterItem,
            this.docLayoutGroup});
            this.layoutGroup.Name = "Root";
            this.layoutGroup.Size = new System.Drawing.Size(1200, 540);
            this.layoutGroup.TextVisible = false;
            // 
            // gridLayoutGroup
            // 
            this.gridLayoutGroup.GroupBordersVisible = false;
            this.gridLayoutGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.gridLayoutItem});
            this.gridLayoutGroup.Location = new System.Drawing.Point(0, 0);
            this.gridLayoutGroup.Name = "gridLayoutGroup";
            this.gridLayoutGroup.Size = new System.Drawing.Size(1180, 145);
            // 
            // gridLayoutItem
            // 
            this.gridLayoutItem.Control = this.grid;
            this.gridLayoutItem.Location = new System.Drawing.Point(0, 0);
            this.gridLayoutItem.Name = "gridLayoutItem";
            this.gridLayoutItem.Size = new System.Drawing.Size(1180, 145);
            this.gridLayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.gridLayoutItem.TextVisible = false;
            // 
            // splitterItem
            // 
            this.splitterItem.AllowHotTrack = true;
            this.splitterItem.Location = new System.Drawing.Point(0, 145);
            this.splitterItem.Name = "splitterItem";
            this.splitterItem.ShowSplitGlyph = DevExpress.Utils.DefaultBoolean.True;
            this.splitterItem.Size = new System.Drawing.Size(1180, 10);
            // 
            // docLayoutGroup
            // 
            this.docLayoutGroup.CustomizationFormText = "docLayoutGroup";
            this.docLayoutGroup.GroupBordersVisible = false;
            this.docLayoutGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.docContentLayoutGroup,
            this.docSplitterItem,
            this.infoLayoutGroup});
            this.docLayoutGroup.Location = new System.Drawing.Point(0, 155);
            this.docLayoutGroup.Name = "docLayoutGroup";
            this.docLayoutGroup.Size = new System.Drawing.Size(1180, 365);
            // 
            // docContentLayoutGroup
            // 
            this.docContentLayoutGroup.GroupBordersVisible = false;
            this.docContentLayoutGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.txLayoutItem,
            this.docxLayoutItem,
            this.richEditLayoutItem,
            this.txLabelItem,
            this.docxLabelItem,
            this.richEditLabelItem});
            this.docContentLayoutGroup.LayoutMode = DevExpress.XtraLayout.Utils.LayoutMode.Table;
            this.docContentLayoutGroup.Location = new System.Drawing.Point(0, 170);
            this.docContentLayoutGroup.Name = "docContentLayoutGroup";
            columnDefinition1.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition1.Width = 100D;
            columnDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition2.Width = 100D;
            columnDefinition3.SizeType = System.Windows.Forms.SizeType.Percent;
            columnDefinition3.Width = 100D;
            this.docContentLayoutGroup.OptionsTableLayoutGroup.ColumnDefinitions.AddRange(new DevExpress.XtraLayout.ColumnDefinition[] {
            columnDefinition1,
            columnDefinition2,
            columnDefinition3});
            rowDefinition1.Height = 17D;
            rowDefinition1.SizeType = System.Windows.Forms.SizeType.AutoSize;
            rowDefinition2.Height = 100D;
            rowDefinition2.SizeType = System.Windows.Forms.SizeType.Percent;
            this.docContentLayoutGroup.OptionsTableLayoutGroup.RowDefinitions.AddRange(new DevExpress.XtraLayout.RowDefinition[] {
            rowDefinition1,
            rowDefinition2});
            this.docContentLayoutGroup.Size = new System.Drawing.Size(1180, 195);
            // 
            // txLayoutItem
            // 
            this.txLayoutItem.Control = this.txTextControl;
            this.txLayoutItem.Location = new System.Drawing.Point(0, 17);
            this.txLayoutItem.Name = "txLayoutItem";
            this.txLayoutItem.OptionsTableLayoutItem.RowIndex = 1;
            this.txLayoutItem.Size = new System.Drawing.Size(393, 178);
            this.txLayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.txLayoutItem.TextVisible = false;
            // 
            // docxLayoutItem
            // 
            this.docxLayoutItem.Control = this.docxTextControl;
            this.docxLayoutItem.Location = new System.Drawing.Point(393, 17);
            this.docxLayoutItem.Name = "docxLayoutItem";
            this.docxLayoutItem.OptionsTableLayoutItem.ColumnIndex = 1;
            this.docxLayoutItem.OptionsTableLayoutItem.RowIndex = 1;
            this.docxLayoutItem.Size = new System.Drawing.Size(393, 178);
            this.docxLayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.docxLayoutItem.TextVisible = false;
            // 
            // richEditLayoutItem
            // 
            this.richEditLayoutItem.Control = this.richEdit;
            this.richEditLayoutItem.Location = new System.Drawing.Point(786, 17);
            this.richEditLayoutItem.Name = "richEditLayoutItem";
            this.richEditLayoutItem.OptionsTableLayoutItem.ColumnIndex = 2;
            this.richEditLayoutItem.OptionsTableLayoutItem.RowIndex = 1;
            this.richEditLayoutItem.Size = new System.Drawing.Size(394, 178);
            this.richEditLayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.richEditLayoutItem.TextVisible = false;
            // 
            // txLabelItem
            // 
            this.txLabelItem.AllowHotTrack = false;
            this.txLabelItem.Location = new System.Drawing.Point(0, 0);
            this.txLabelItem.Name = "txLabelItem";
            this.txLabelItem.Size = new System.Drawing.Size(393, 17);
            this.txLabelItem.Text = "TXTextControl (Original format)";
            this.txLabelItem.TextSize = new System.Drawing.Size(151, 13);
            // 
            // docxLabelItem
            // 
            this.docxLabelItem.AllowHotTrack = false;
            this.docxLabelItem.Location = new System.Drawing.Point(393, 0);
            this.docxLabelItem.Name = "docxLabelItem";
            this.docxLabelItem.OptionsTableLayoutItem.ColumnIndex = 1;
            this.docxLabelItem.Size = new System.Drawing.Size(393, 17);
            this.docxLabelItem.Text = "TXTextControl (DOCX format)";
            this.docxLabelItem.TextSize = new System.Drawing.Size(151, 13);
            // 
            // richEditLabelItem
            // 
            this.richEditLabelItem.AllowHotTrack = false;
            this.richEditLabelItem.Location = new System.Drawing.Point(786, 0);
            this.richEditLabelItem.Name = "richEditLabelItem";
            this.richEditLabelItem.OptionsTableLayoutItem.ColumnIndex = 2;
            this.richEditLabelItem.Size = new System.Drawing.Size(394, 17);
            this.richEditLabelItem.Text = "RichEditControl (DOCX format)";
            this.richEditLabelItem.TextSize = new System.Drawing.Size(151, 13);
            // 
            // docSplitterItem
            // 
            this.docSplitterItem.AllowHotTrack = true;
            this.docSplitterItem.FixedStyle = DevExpress.XtraLayout.SplitterItemFixedStyles.LeftTop;
            this.docSplitterItem.Location = new System.Drawing.Point(0, 160);
            this.docSplitterItem.Name = "docSplitterItem";
            this.docSplitterItem.ShowSplitGlyph = DevExpress.Utils.DefaultBoolean.True;
            this.docSplitterItem.Size = new System.Drawing.Size(1180, 10);
            // 
            // infoLayoutGroup
            // 
            this.infoLayoutGroup.GroupBordersVisible = false;
            this.infoLayoutGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.hexLayoutGroup,
            this.fieldLayoutGroup,
            this.infoSplitterItem});
            this.infoLayoutGroup.Location = new System.Drawing.Point(0, 0);
            this.infoLayoutGroup.Name = "infoLayoutGroup";
            this.infoLayoutGroup.Size = new System.Drawing.Size(1180, 160);
            this.infoLayoutGroup.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.infoLayoutGroup.Text = "Info";
            this.infoLayoutGroup.TextVisible = false;
            // 
            // hexLayoutGroup
            // 
            this.hexLayoutGroup.AppearanceGroup.Options.UseTextOptions = true;
            this.hexLayoutGroup.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.hexLayoutGroup.GroupStyle = DevExpress.Utils.GroupStyle.Title;
            this.hexLayoutGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.hexLayoutItem});
            this.hexLayoutGroup.Location = new System.Drawing.Point(0, 0);
            this.hexLayoutGroup.Name = "hexLayoutGroup";
            this.hexLayoutGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.hexLayoutGroup.Size = new System.Drawing.Size(1180, 52);
            this.hexLayoutGroup.Text = "HEX";
            this.hexLayoutGroup.TextLocation = DevExpress.Utils.Locations.Left;
            // 
            // hexLayoutItem
            // 
            this.hexLayoutItem.Control = this.hexGrid;
            this.hexLayoutItem.Location = new System.Drawing.Point(0, 0);
            this.hexLayoutItem.Name = "hexLayoutItem";
            this.hexLayoutItem.Size = new System.Drawing.Size(1150, 48);
            this.hexLayoutItem.Text = "Hex";
            this.hexLayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.hexLayoutItem.TextVisible = false;
            // 
            // fieldLayoutGroup
            // 
            this.fieldLayoutGroup.AppearanceGroup.Options.UseTextOptions = true;
            this.fieldLayoutGroup.AppearanceGroup.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.fieldLayoutGroup.GroupStyle = DevExpress.Utils.GroupStyle.Title;
            this.fieldLayoutGroup.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.fieldLayoutItem});
            this.fieldLayoutGroup.Location = new System.Drawing.Point(0, 62);
            this.fieldLayoutGroup.Name = "fieldLayoutGroup";
            this.fieldLayoutGroup.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.fieldLayoutGroup.Size = new System.Drawing.Size(1180, 98);
            this.fieldLayoutGroup.Text = "FIELDS";
            this.fieldLayoutGroup.TextLocation = DevExpress.Utils.Locations.Left;
            // 
            // fieldLayoutItem
            // 
            this.fieldLayoutItem.Control = this.fieldGrid;
            this.fieldLayoutItem.Location = new System.Drawing.Point(0, 0);
            this.fieldLayoutItem.Name = "fieldLayoutItem";
            this.fieldLayoutItem.Size = new System.Drawing.Size(1150, 94);
            this.fieldLayoutItem.Text = "Fields";
            this.fieldLayoutItem.TextSize = new System.Drawing.Size(0, 0);
            this.fieldLayoutItem.TextVisible = false;
            // 
            // infoSplitterItem
            // 
            this.infoSplitterItem.AllowHotTrack = true;
            this.infoSplitterItem.FixedStyle = DevExpress.XtraLayout.SplitterItemFixedStyles.LeftTop;
            this.infoSplitterItem.Location = new System.Drawing.Point(0, 52);
            this.infoSplitterItem.Name = "infoSplitterItem";
            this.infoSplitterItem.ShowSplitGlyph = DevExpress.Utils.DefaultBoolean.True;
            this.infoSplitterItem.Size = new System.Drawing.Size(1180, 10);
            // 
            // toolbarFormControl1
            // 
            this.toolbarFormControl1.Location = new System.Drawing.Point(0, 0);
            this.toolbarFormControl1.Manager = this.toolbarFormManager;
            this.toolbarFormControl1.Name = "toolbarFormControl1";
            this.toolbarFormControl1.Size = new System.Drawing.Size(1200, 30);
            this.toolbarFormControl1.TabIndex = 1;
            this.toolbarFormControl1.TabStop = false;
            this.toolbarFormControl1.TitleItemLinks.Add(this.saveDocxBarButtonItem);
            this.toolbarFormControl1.TitleItemLinks.Add(this.saveOrigBarButtonItem, true);
            this.toolbarFormControl1.TitleItemLinks.Add(this.serverBarButtonItem, true);
            this.toolbarFormControl1.TitleItemLinks.Add(this.statusBarEditItem, true);
            this.toolbarFormControl1.TitleItemLinks.Add(this.statusBarStaticItem, true);
            this.toolbarFormControl1.TitleItemLinks.Add(this.vertBarCheckItem);
            this.toolbarFormControl1.TitleItemLinks.Add(this.horzBarCheckItem, true);
            this.toolbarFormControl1.ToolbarForm = this;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1200, 570);
            this.Controls.Add(this.layoutControl);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Controls.Add(this.toolbarFormControl1);
            this.IconOptions.Icon = ((System.Drawing.Icon)(resources.GetObject("MainForm.IconOptions.Icon")));
            this.Name = "MainForm";
            this.Text = "CheckDocs";
            this.ToolbarFormControl = this.toolbarFormControl1;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl)).EndInit();
            this.layoutControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fieldGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fieldGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toolbarFormManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusMarqueeProgressBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hexGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hexSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hexGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.headerRepositoryItemTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLayoutGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.docLayoutGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.docContentLayoutGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txLayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.docxLayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.richEditLayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txLabelItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.docxLabelItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.richEditLabelItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.docSplitterItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.infoLayoutGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hexLayoutGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hexLayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fieldLayoutGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fieldLayoutItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.infoSplitterItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.toolbarFormControl1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl;
        private DevExpress.XtraLayout.LayoutControlGroup layoutGroup;
        private DevExpress.XtraGrid.GridControl grid;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private TXTextControl.TextControl docxTextControl;
        private TXTextControl.TextControl txTextControl;
        private DevExpress.XtraRichEdit.RichEditControl richEdit;
        private DevExpress.XtraLayout.LayoutControlGroup docContentLayoutGroup;
        private DevExpress.XtraLayout.LayoutControlItem txLayoutItem;
        private DevExpress.XtraLayout.LayoutControlItem docxLayoutItem;
        private DevExpress.XtraLayout.LayoutControlItem richEditLayoutItem;
        private DevExpress.XtraLayout.LayoutControlGroup gridLayoutGroup;
        private DevExpress.XtraLayout.LayoutControlItem gridLayoutItem;
        private DevExpress.XtraLayout.SplitterItem splitterItem;
        private DevExpress.XtraLayout.SimpleLabelItem txLabelItem;
        private DevExpress.XtraLayout.SimpleLabelItem docxLabelItem;
        private DevExpress.XtraLayout.SimpleLabelItem richEditLabelItem;
        private DevExpress.Data.Linq.EntityInstantFeedbackSource docsSource;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colCreateDate;
        private DevExpress.XtraGrid.Columns.GridColumn colFilename;
        private DevExpress.XtraGrid.Columns.GridColumn colContextMode;
        private DevExpress.XtraGrid.Columns.GridColumn colIsTemplate;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerID;
        private DevExpress.XtraGrid.Columns.GridColumn colSize;
        private DevExpress.XtraGrid.Columns.GridColumn colCompressed;
        private DevExpress.XtraBars.ToolbarForm.ToolbarFormManager toolbarFormManager;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.ToolbarForm.ToolbarFormControl toolbarFormControl1;
        private DevExpress.XtraBars.BarButtonItem saveDocxBarButtonItem;
        private DevExpress.XtraBars.BarButtonItem saveOrigBarButtonItem;
        private DevExpress.XtraBars.BarStaticItem statusBarStaticItem;
        private DevExpress.XtraBars.BarEditItem statusBarEditItem;
        private DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar statusMarqueeProgressBar;
        private DevExpress.XtraBars.BarButtonItem serverBarButtonItem;
        private DevExpress.XtraBars.BarCheckItem vertBarCheckItem;
        private DevExpress.XtraBars.BarCheckItem horzBarCheckItem;
        private DevExpress.XtraGrid.Columns.GridColumn colHeader;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit headerRepositoryItemTextEdit;
        private DevExpress.XtraGrid.GridControl hexGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView hexGridView;
        private DevExpress.XtraLayout.LayoutControlGroup docLayoutGroup;
        private DevExpress.XtraLayout.LayoutControlItem hexLayoutItem;
        private DevExpress.XtraLayout.SplitterItem docSplitterItem;
        private DevExpress.Data.UnboundSource hexSource;
        private DevExpress.XtraGrid.Columns.GridColumn colPosition;
        private DevExpress.XtraGrid.Columns.GridColumn colByte0;
        private DevExpress.XtraGrid.Columns.GridColumn colByte1;
        private DevExpress.XtraGrid.Columns.GridColumn colByte2;
        private DevExpress.XtraGrid.Columns.GridColumn colByte3;
        private DevExpress.XtraGrid.Columns.GridColumn colByte4;
        private DevExpress.XtraGrid.Columns.GridColumn colByte5;
        private DevExpress.XtraGrid.Columns.GridColumn colByte6;
        private DevExpress.XtraGrid.Columns.GridColumn colByte7;
        private DevExpress.XtraGrid.Columns.GridColumn colByte8;
        private DevExpress.XtraGrid.Columns.GridColumn colByte9;
        private DevExpress.XtraGrid.Columns.GridColumn colByteA;
        private DevExpress.XtraGrid.Columns.GridColumn colByteB;
        private DevExpress.XtraGrid.Columns.GridColumn colByteC;
        private DevExpress.XtraGrid.Columns.GridColumn colByteD;
        private DevExpress.XtraGrid.Columns.GridColumn colByteE;
        private DevExpress.XtraGrid.Columns.GridColumn colByteF;
        private DevExpress.XtraGrid.Columns.GridColumn colText;
        private DevExpress.XtraLayout.LayoutControlGroup hexLayoutGroup;
        private DevExpress.XtraGrid.GridControl fieldGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView fieldGridView;
        private DevExpress.XtraLayout.LayoutControlGroup infoLayoutGroup;
        private DevExpress.XtraLayout.LayoutControlGroup fieldLayoutGroup;
        private DevExpress.XtraLayout.LayoutControlItem fieldLayoutItem;
        private DevExpress.XtraLayout.SplitterItem infoSplitterItem;
        private DevExpress.XtraGrid.Columns.GridColumn fieldIDColumn;
        private DevExpress.XtraGrid.Columns.GridColumn fieldNameColumn;
        private DevExpress.XtraGrid.Columns.GridColumn fieldTextColumn;
        private DevExpress.XtraGrid.Columns.GridColumn fieldStartColumn;
        private DevExpress.XtraGrid.Columns.GridColumn fieldLengthColumn;
    }
}