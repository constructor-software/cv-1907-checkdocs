﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckDocs
{

    [Table("tblDocumentBase")]
    public class DocumentBase
    {

        [Column("document_id")]
        public Guid ID { get; set; }

        [Column("Create_Date")]
        public DateTime CreateDate { get; set; }

        [Column("filename")]
        public string Filename { get; set; }

        [Column("context_mode")]
        public string ContextMode { get; set; }

        [Column("is_template")]
        public bool IsTemplate { get; set; }

        [Column("owner_id")]
        public Guid? OwnerID { get; set; }

        [Column("document_file")]
        public byte[] Content { get; set; }

    }

}
