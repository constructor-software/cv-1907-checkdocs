﻿using DevExpress.XtraEditors;
using DevExpress.XtraLayout.Utils;
using Microsoft.SqlServer.Management.Smo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Constructor.WinUI
{

    /// <summary>
    /// Dialog that shows the user a list of detected SQL Server instances for selection.
    /// </summary>
    public partial class SelectServerDialog : XtraForm
    {

        #region Classes

        /// <summary>
        /// Sql Server instance details.
        /// </summary>
        public class SqlServerInstance
        {
            private const string DefaultInstance = "Constructor";

            public string Name { get; private set; }
            public string Instance { get; private set; }
            public bool IsLocal { get; private set; }

            public bool IsDefault => !string.IsNullOrEmpty(Instance) && Instance.Equals(DefaultInstance, StringComparison.InvariantCultureIgnoreCase);

            public string DisplayName => IsLocal ? @"(LOCAL)\" + Instance : Name;

            public SqlServerInstance(string name, string instance, bool isLocal)
            {
                Name = name;
                Instance = instance;
                IsLocal = isLocal;
            }
        }

        #endregion

        #region Properties

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string ServerName
        {
            get => serverNameTextEdit.Text;
            set => serverNameTextEdit.Text = value;
        }

        #endregion

        #region Constructors

        public SelectServerDialog()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods

        protected async override void OnShown(EventArgs e)
        {
            base.OnShown(e);

            serverNameTextEdit.Select();

            await EnumAvailableSqlServersAsync();
        }

        /// <summary>
        /// Searches the network for SQL Server instances in the background, and then lists them for selection.
        /// </summary>
        private async Task EnumAvailableSqlServersAsync()
        {
            var serverName = ServerName;

            try
            {
                var serverTable = await Task.Run(() => {
                    return SmoApplication.EnumAvailableSqlServers();
                });
                if (IsDisposed) return;

                var instances = (
                    from dr in serverTable.Rows.Cast<DataRow>()
                    let i = new SqlServerInstance(
                        dr.Field<string>("Name"),
                        dr.Field<string>("Instance"),
                        dr.Field<bool>("IsLocal"))
                    orderby i.IsLocal descending, i.IsDefault descending, i.DisplayName
                    select i
                    ).ToList();

                serverImageListBox.SelectedIndexChanged -= serverImageListBox_SelectedIndexChanged;
                instanceBindingSource.DataSource = instances;
                layoutControl.BeginUpdate();
                try
                {
                    serverLayoutItem.Visibility = LayoutVisibility.Always;
                    progressLayoutItem.Visibility = LayoutVisibility.Never;
                }
                finally
                {
                    layoutControl.EndUpdate();
                    serverImageListBox.SelectedIndexChanged += serverImageListBox_SelectedIndexChanged;
                }

                var index = instances.FindIndex(i => string.Equals(i.DisplayName, serverName, StringComparison.OrdinalIgnoreCase));
                if (index >= 0)
                    serverImageListBox.SelectedIndex = index;
                else
                    serverImageListBox.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                if (IsDisposed) return;
                XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void serverImageListBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            var index = serverImageListBox.IndexFromPoint(e.Location);
            if (index < 0)
                return;

            okButton.PerformClick();
        }

        private void serverImageListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (serverImageListBox.SelectedIndex < 0) return;
            if (serverImageListBox.SelectedItem is SqlServerInstance instance)
            {
                serverNameTextEdit.Text = instance.DisplayName;
                serverNameTextEdit.SelectAll();
            }
        }

        #endregion

    }
}
