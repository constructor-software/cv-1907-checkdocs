﻿using EntityFramework.Functions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckDocs
{

    public class DocsDbContext : DbContext
    {

        #region Properties

        public DbSet<DocumentBase> DocumentBases { get; set; }

        #endregion

        #region Constructors

        public DocsDbContext(string server) : base(GetConnectionString(server))
        {
            Database.SetInitializer<DocsDbContext>(null);

#if DEBUG
            // Log the commands in Debug mode
            Database.Log = LogForDebug;
#endif
        }

        #endregion

        #region Methods

        private static string GetConnectionString(string server)
        {
            var builder = new SqlConnectionStringBuilder();
            builder.DataSource = server;
            builder.IntegratedSecurity = false;
            builder.UserID = "constructor";
            builder.Password = "letme1n#";
            builder.InitialCatalog = "ConstructorNet";
            builder.MultipleActiveResultSets = true;
            return builder.ToString();
        }

#if DEBUG
        /// <summary>
        /// Callback method used to log the SQL commands being issued by the Entity Framework.
        /// </summary>
        /// <param name="message"></param>
        private void LogForDebug(string message)
        {
            if (string.IsNullOrWhiteSpace(message))
                return;

            Debug.WriteLine($"[DocsDbContext 0x{GetHashCode():X8}]\n{message.Trim()}");
        }
#endif

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.AddFunctions(typeof(BuiltInFunctions));
        }

        #endregion

    }

    public static class BuiltInFunctions
    {

        [Function(FunctionType.BuiltInFunction, "SUBSTRING")]
        public static byte[] Substring(byte[] value, int? index, int? length)
            => Function.CallNotSupported<byte[]>();
        
    }

}
